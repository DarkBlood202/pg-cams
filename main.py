import sys
from random import randint

import pygame as pg


def clamp(n, smallest, largest):
    return max(smallest, min(n, largest))


class Tree(pg.sprite.Sprite):
    def __init__(self, pos, group) -> None:
        super().__init__(group)
        # self.image = pg.image.load("tree.png").convert_alpha()
        self.image = pg.Surface((32, 64))
        self.image.fill("green")
        self.rect = self.image.get_rect(topleft=pos)


class Player(pg.sprite.Sprite):
    def __init__(self, pos, group) -> None:
        super().__init__(group)
        # self.image = pg.image.load("player.png").convert_alpha()
        self.image = pg.Surface((32, 32))
        self.image.fill("blue")
        self.rect = self.image.get_rect(center=pos)
        self.direction = pg.math.Vector2()
        self.speed = 4

    def input(self):
        keys = pg.key.get_pressed()

        if keys[pg.K_UP]:
            self.direction.y = -1
        elif keys[pg.K_DOWN]:
            self.direction.y = 1
        else:
            self.direction.y = 0

        if keys[pg.K_RIGHT]:
            self.direction.x = 1
        elif keys[pg.K_LEFT]:
            self.direction.x = -1
        else:
            self.direction.x = 0

    def update(self):
        self.input()
        self.rect.center += self.direction * self.speed


class CameraGroup(pg.sprite.Group):
    def __init__(self) -> None:
        super().__init__()
        self.display_surface = pg.display.get_surface()

        # cam offsest
        self.offset = pg.math.Vector2((300, 200))
        self.half_w = self.display_surface.get_size()[0] // 2
        self.half_h = self.display_surface.get_size()[1] // 2

        # box setup
        self.camera_borders = {"left": 200, "right": 200, "top": 100, "bottom": 100}
        l = self.camera_borders["left"]
        t = self.camera_borders["top"]
        w = self.display_surface.get_size()[0] - (
            self.camera_borders["left"] + self.camera_borders["right"]
        )
        h = self.display_surface.get_size()[1] - (
            self.camera_borders["top"] + self.camera_borders["bottom"]
        )
        self.camera_rect = pg.Rect(l, t, w, h)

        # self.ground_surface = pg.image.load("ground.png").convert_alpha()
        self.ground_surface = pg.Surface((1920, 1080))
        self.ground_surface.fill("red")
        self.ground_rect = self.ground_surface.get_rect(topleft=(0, 0))

        # cam speed
        self.keyboard_speed = 5
        self.mouse_speed = 0.4

        # zoom
        self.zoom_scale = 1
        self.internal_surface_size = (2500, 2500)
        self.internal_surface = pg.Surface(self.internal_surface_size, pg.SRCALPHA)
        self.internal_rect = self.internal_surface.get_rect(
            center=(self.half_w, self.half_h)
        )
        self.internal_surface_size_vector = pg.math.Vector2(self.internal_surface_size)

        self.internal_offset = pg.math.Vector2()
        self.internal_offset.x = self.internal_surface_size[0] // 2 - self.half_w
        self.internal_offset.y = self.internal_surface_size[1] // 2 - self.half_h

        self.min_zoom = 0.5
        self.max_zoom = 2

    def center_target_camera(self, target):
        self.offset.x = target.rect.centerx - self.half_w
        self.offset.y = target.rect.centery - self.half_h

    def box_target_camera(self, target):
        if target.rect.left < self.camera_rect.left:
            self.camera_rect.left = target.rect.left

        if target.rect.right > self.camera_rect.right:
            self.camera_rect.right = target.rect.right

        if target.rect.top < self.camera_rect.top:
            self.camera_rect.top = target.rect.top

        if target.rect.bottom > self.camera_rect.bottom:
            self.camera_rect.bottom = target.rect.bottom

        self.offset.x = self.camera_rect.left - self.camera_borders["left"]
        self.offset.y = self.camera_rect.top - self.camera_borders["top"]

    def keyboard_control(self):
        keys = pg.key.get_pressed()

        if keys[pg.K_a]:
            self.offset.x -= self.keyboard_speed
        elif keys[pg.K_d]:
            self.offset.x += self.keyboard_speed

        if keys[pg.K_w]:
            self.offset.y -= self.keyboard_speed
        elif keys[pg.K_s]:
            self.offset.y += self.keyboard_speed

    def keyboard_control_box(self):
        keys = pg.key.get_pressed()

        if keys[pg.K_a]:
            self.camera_rect.x -= self.keyboard_speed
        elif keys[pg.K_d]:
            self.camera_rect.x += self.keyboard_speed

        if keys[pg.K_w]:
            self.camera_rect.y -= self.keyboard_speed
        elif keys[pg.K_s]:
            self.camera_rect.y += self.keyboard_speed

        self.offset.x = self.camera_rect.left - self.camera_borders["left"]
        self.offset.y = self.camera_rect.top - self.camera_borders["top"]

    def mouse_control(self):
        mouse = pg.math.Vector2(pg.mouse.get_pos())
        mouse_offset_vector = pg.math.Vector2()

        left_border = self.camera_borders["left"]
        top_border = self.camera_borders["top"]
        right_border = self.display_surface.get_size()[0] - self.camera_borders["right"]
        bot_border = self.display_surface.get_size()[1] - self.camera_borders["bottom"]

        if top_border < mouse.y < bot_border:
            if mouse.x < left_border:
                mouse_offset_vector.x = mouse.x - left_border
                pg.mouse.set_pos((left_border, mouse.y))
            if mouse.x > right_border:
                mouse_offset_vector.x = mouse.x - right_border
                pg.mouse.set_pos((right_border, mouse.y))
        elif mouse.y < top_border:
            if mouse.x < left_border:
                mouse_offset_vector = mouse - pg.math.Vector2((left_border, top_border))
                pg.mouse.set_pos((left_border, top_border))
            if mouse.x > right_border:
                mouse_offset_vector = mouse - pg.math.Vector2(right_border, top_border)
                pg.mouse.set_pos((right_border, top_border))
        elif mouse.y > bot_border:
            if mouse.x < left_border:
                mouse_offset_vector = mouse - pg.math.Vector2((left_border, bot_border))
                pg.mouse.set_pos((left_border, bot_border))
            if mouse.x > right_border:
                mouse_offset_vector = mouse - pg.math.Vector2(right_border, bot_border)
                pg.mouse.set_pos((right_border, bot_border))

        if left_border < mouse.x < right_border:
            if mouse.y < top_border:
                mouse_offset_vector.y = mouse.y - top_border
                pg.mouse.set_pos((mouse.x, top_border))
            if mouse.y > bot_border:
                mouse_offset_vector.y = mouse.y - bot_border
                pg.mouse.set_pos((mouse.x, bot_border))

        self.offset += mouse_offset_vector * self.mouse_speed

    def zoom_keyboard_control(self):
        keys = pg.key.get_pressed()

        if keys[pg.K_q]:
            self.zoom_scale += 0.1
        elif keys[pg.K_e]:
            self.zoom_scale -= 0.1

    def custom_draw(self):
        # self.center_target_camera(player)
        # self.box_target_camera(player)
        # self.keyboard_control()
        # self.keyboard_control_box()
        self.mouse_control()
        # self.zoom_keyboard_control()

        self.internal_surface.fill("#180c21")

        # ground
        ground_offset = self.ground_rect.topleft - self.offset + self.internal_offset
        self.internal_surface.blit(self.ground_surface, ground_offset)

        # active elements
        for sprite in sorted(self.sprites(), key=lambda sprite: sprite.rect.centery):
            offset_pos = sprite.rect.topleft - self.offset + self.internal_offset
            self.internal_surface.blit(sprite.image, offset_pos)

        scaled_surf = pg.transform.scale(
            self.internal_surface, self.internal_surface_size_vector * self.zoom_scale
        )
        scaled_rect = scaled_surf.get_rect(center=(self.half_w, self.half_h))

        self.display_surface.blit(scaled_surf, scaled_rect)


pg.init()
screen = pg.display.set_mode((800, 600))
clock = pg.time.Clock()
pg.event.set_grab(True)

camera_group = CameraGroup()
player = Player((640, 360), camera_group)

for i in range(20):
    r_x = randint(600, 1200)
    r_y = randint(0, 1000)

    Tree((r_x, r_y), camera_group)

while True:
    clock.tick(60)

    for ev in pg.event.get():
        if ev.type == pg.QUIT:
            pg.quit()
            sys.exit()
        if ev.type == pg.KEYDOWN:
            if ev.key == pg.K_ESCAPE:
                pg.quit()
                sys.exit()

        if ev.type == pg.MOUSEWHEEL:
            camera_group.zoom_scale = clamp(
                camera_group.zoom_scale, camera_group.min_zoom, camera_group.max_zoom
            )
            camera_group.zoom_scale += ev.y * 0.03

            print(camera_group.zoom_scale)

    screen.fill("#180c21")

    camera_group.update()
    camera_group.custom_draw()

    pg.display.flip()
